

export const dataReponse = [
    {
        id: "a123",
        imgLink: "http://placehold.it/600x300",
        imgName: "Some Test Content 1",
        title: "Some Test Content 1",
        summary: "Cras ultricies ligula sed magna dictum porta. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Curabitur aliquet quam id dui posuere blandit.",
        modal: {
            id: "m1",
            label: "Modal 1",
            title: "Modal title 1",
            text: "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Curabitur aliquet quam id dui posuere blandit. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Mauris blandit aliquet elit"
        }
    },
    {
        id: "b13",
        imgLink: "http://placehold.it/600x300",
        imgName: "Some Test Content 2",
        title: "Some Test Content 2",
        summary: "Pellentesque in ipsum id orci porta dapibus.",
        modal: {
            id: "m2",
            label: "Modal 2",
            title: "Modal title 2",
            text: "ipsum id orci porta dapibus Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Mauris blandit aliquet elit"
        }
    },
    {
        id: "sdf34",
        imgLink: "http://placehold.it/600x300",
        imgName: "Some Test Content 3",
        title: "Some Test Content 3",
        summary: "Pellentesque in ipsum id orci porta dapibus. Nulla quis lorem ut libero malesuada feugiat.",
        modal: {
            id: "m3",
            label: "Modal 3",
            title: "Modal title 3",
            text: "Nulla quis lorem ut libero malesuada feugiat. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Mauris blandit aliquet elit"
        }
    },
    // {
    //     id: "sdf45",
    //     imgLink: "http://placehold.it/600x300",
    //     imgName: "Some Test Content 4",
    //     title: "Some Test Content 4",
    //     summary: "Donec rutrum congue leo eget malesuada.",
    //     modal: {
    //         id: "m4",
    //         label: "Modal 4",
    //         title: "Modal title 4",
    //         text: "Donec rutrum congue leo eget malesuada Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Mauris blandit aliquet elit"
    //     }
    // }
];


