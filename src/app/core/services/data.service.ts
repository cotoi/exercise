import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/map';

import { HttpClient } from '@angular/common/http';
import * as mockData from '../../../testing';
import { Card } from '../../cards-feature/models/card';
import { Modal } from '../../cards-feature/models/modal';

interface TNSResponse {
  Count: number,
  Items: [
    {
      Id: number,
      Name: string,
      Desc: string,
      ShortDesc: string,
      Media: {
        Id: number,
        Name: string,
        Type: string,
        InfoUrl: string,
        Images: [
          {
            Type: string,
            Url: string,
            Width: number,
            Height: number
          }
        ]
      },
      Images: [
        {
          Type: string,
          Url: string,
          Width: number,
          Height: number
        }
      ]
    }
  ],
  ItemsType: string
}


@Injectable()
export class DataService {

  url = 'http://capi.9c9media.com/destinations/tsn_web/platforms/desktop/collections/72/contents?$include=[Images,Desc,ShortDesc,Media]&$page=1&$top=25&$inlinecount=&Images.Type=thumbnail';

  constructor(private http: HttpClient) { }

  getData(): Observable<Card[]> {
    return this.http.get<TNSResponse>(this.url).map(this.mapToCard);
    // Here we would actually get some real data 
    //return of(mockData.dataReponse);
  }

  /**
   * Map items from TNS API to Card
   * @param data 
   */
  mapToCard(data: TNSResponse): Card[] {
    return data.Items.map(item => new Card(
      item.Id.toString(),
      item.Images[0].Url,
      item.Media.Name,
      item.Name,
      item.Desc,
      new Modal(
        item.Media.Id.toString(),
        item.Media.Type,
        item.Media.Name, 
        item.Name
      )
    ));
  }

}
