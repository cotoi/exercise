import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Card } from '../../models/card';
import { Modal } from '../../models/modal';
import { trigger, state, style, transition, animate, query } from '@angular/animations';

@Component({
  selector: 'npw-card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.scss'],
  animations: [
    trigger('fade', [
      transition('* => *', [

        query(':enter',
          [
            style({ opacity: 0 })
          ],
          { optional: true }
        ),

        query(':leave',
          [
            style({ opacity: 1 }),
            animate('0.4s', style({ opacity: 0 }))
          ],
          { optional: true }
        ),

        query(':enter',
          [
            style({ opacity: 0 }),
            animate('0.4s', style({ opacity: 1 }))
          ],
          { optional: true }
        )

      ])

    ])


  ]
})
export class CardListComponent implements OnInit {

  @Input() cards: Card[];
  @Output() modalClicked:EventEmitter<Modal> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
