import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Card } from '../../models/card';
import { Modal } from '../../models/modal';

@Component({
  selector: 'npw-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() data: Card;
  @Output() modalClick:EventEmitter<Modal> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  get image(){
    return this.data.imgLink;
  }

  get imageName(){
    return this.data.imgName;
  }

  get title(){
    return this.data.title;
  }

  get summary(){
    return this.data.summary;
  }

  get modal(){
    return this.data.modal;
  }

}
