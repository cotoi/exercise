import { Action } from '@ngrx/store';
import { Card } from '../models/card';


export const LOAD = '[Cards] Load';
export const LOAD_SUCCESS = '[Cards] Load Success';
export const LOAD_FAIL = '[Cards] Load Fail';




/**
 * Load Actions
 */
export class Load implements Action {
  readonly type = LOAD;
}

export class LoadSuccess implements Action {
  readonly type = LOAD_SUCCESS;

  constructor(public payload: Card[]) {}
}

export class LoadFail implements Action {
  readonly type = LOAD_FAIL;

  constructor(public payload: any) {}
}

export type Actions =
  | Load
  | LoadSuccess
  | LoadFail;
