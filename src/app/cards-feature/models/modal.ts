export class Modal {
    constructor(
        public id: string,
        public label: string,
        public title: string,
        public text: string   
    ){}
}
