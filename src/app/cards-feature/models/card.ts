import { Modal } from "./modal";

export class Card {
    constructor(    
        public id:string,
        public imgLink:string,
        public imgName:string,
        public title:string,
        public summary:string,
        public modal:Modal
    ){};
}
