import { Component, OnInit, ChangeDetectionStrategy, EventEmitter, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Card } from '../../models/card';
import * as cardsAction from '../../actions/cards';
import * as fromCards from '../../reducers';
import { Modal } from '../../models/modal';

@Component({
  selector: 'npw-listing-page',
  templateUrl: './listing-page.component.html',
  styleUrls: ['./listing-page.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListingPageComponent implements OnInit {
  cards$: Observable<Card[]>;
  @Output() onModalClicked: EventEmitter<Modal> = new EventEmitter()

  constructor(private store: Store<fromCards.State>) {
    this.cards$ = store.select(fromCards.getCardsCollection);
  }

  ngOnInit() {
    // we can load from here or from @effect
    //this.store.dispatch(new cardsAction.Load());
  }

}
