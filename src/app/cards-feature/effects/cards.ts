import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/toArray';
import 'rxjs/add/operator/startWith';
import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { defer } from 'rxjs/observable/defer';
import { of } from 'rxjs/observable/of';

import * as cards from '../actions/cards';
import { Card } from '../models/card';
import { DataService } from '../../core/services/data.service';

@Injectable()
export class CardsEffects {


    constructor(private actions$: Actions, private dataService:DataService) { }

    @Effect()
    loadCards$: Observable<Action> = this.actions$
        .ofType(cards.LOAD)
        .startWith(new cards.Load()) // Get data immediately
        .switchMap(() =>
            this.dataService
                .getData()
                .map((cardsList: Card[]) => new cards.LoadSuccess(cardsList))
                .catch(error => of(new cards.LoadFail(error)))
        );


}
