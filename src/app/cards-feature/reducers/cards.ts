import * as cards from '../actions/cards';
import { Card } from '../models/card';

export interface State {
  loaded: boolean;
  loading: boolean;
  ids: string[];
  entities: { [id: string]: Card };
}

export const initialState: State = {
  loaded: false,
  loading: false,
  ids: [],
  entities: {}
};

export function reducer( state = initialState, action: cards.Actions): State {
  
  switch (action.type) {

    case cards.LOAD: {
      return {
        ...state,
        loading: true,
      };
    }

    case cards.LOAD_SUCCESS: {

      const cards = action.payload;
      const newCards = cards.filter(card => !state.entities[card.id]);

      const newCardIds = newCards.map(card => card.id);
      const newCardEntities = newCards.reduce((entities: { [id: string]: Card }, card: Card) => {
        return Object.assign(entities, {
          [card.id]: card
        });
      }, {});

      return {
        loaded: true,
        loading: false,
        ids: [...state.ids, ...newCardIds],
        entities: Object.assign({}, state.entities, newCardEntities),
      };
    }

    default: {
      return state;
    }
  }
}

export const getLoaded = (state: State) => state.loaded;

export const getLoading = (state: State) => state.loading;

export const getIds = (state: State) => state.ids;

export const getEntities = (state: State) => state.entities;
