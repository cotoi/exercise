import { reducer } from './cards';
import * as fromCards from './cards'
import * as mockData from '../../../testing';
import { Card } from '../models/card';
import * as cardActions from '../actions/cards'

describe('Cards Reducer', () => {


    describe('undefined action', () => {
        it('should return the default state', () => {
            const action = {} as any;

            const result = reducer(undefined, action);

            expect(result).toEqual(fromCards.initialState);
        });
    });


    describe('Selections', () => {
        const card1 = Object.assign({}, mockData.dataReponse[0], { id: '111' }) as Card;
        const card2 = Object.assign({}, mockData.dataReponse[1], { id: '222' }) as Card;

        const state: fromCards.State = {
            ids: [card1.id, card2.id],
            entities: {
                '111': card1,
                '222': card2,
            },
            loaded: false,
            loading: true

        };

        describe('getEntities', () => {
            it('should return entities', () => {
                const result = fromCards.getEntities(state);
                expect(result).toBe(state.entities);
            });
        });

        describe('getIds', () => {
            it('should return ids', () => {
                const result = fromCards.getIds(state);
                expect(result).toBe(state.ids);
            });
        });

        describe('getLoaded', () => {
            it('should return loaded state false ', () => {
                const result = fromCards.getLoaded(state);
                expect(result).toBe(false);
            });
        });

        describe('getLoading', () => {
            it('should return loading state true ', () => {
                const result = fromCards.getLoading(state);
                expect(result).toBe(true);
            });
        });
    });

    describe('LOAD_SUCCESS', () => {

        it('should add all the cards in the payload when none exist', () => { 
            // Arrange
            const card1 = Object.assign({}, mockData.dataReponse[0], { id: '111' }) as Card;
            const card2 = Object.assign({}, mockData.dataReponse[1], { id: '222' }) as Card;

            const expectedResult = {
                ids: ['111', '222'],
                entities: {
                    '111': card1,
                    '222': card2
                },
                loaded: true,
                loading: false
            };

            const createAction = new cardActions.LoadSuccess([card1, card2]);

            // Act
            const result = reducer(fromCards.initialState, createAction);
          
            //Assert
            expect(result).toEqual(expectedResult);
          });
      
          it('should only add new cards when some already exist', () => {
            // Arrange
            const card1 = Object.assign({}, mockData.dataReponse[0], { id: '111' }) as Card;
            const card2 = Object.assign({}, mockData.dataReponse[1], { id: '222' }) as Card;

            const initialState = {
                ids: ['111', '222'],
                entities: {
                    '111': card1,
                    '222': card2
                },
                loaded: true,
                loading: false
            };


            const card3 = Object.assign({}, mockData.dataReponse[2], { id: '333' }) as Card;
            const card4Existing = Object.assign({}, mockData.dataReponse[0], { id: '111' }) as Card;

            const expectedResult = {
                ids: ['111', '222', '333'],
                entities: {
                    '111': card1,
                    '222': card2,
                    '333': card3
                },
                loaded: true,
                loading: false
            };

            const createAction = new cardActions.LoadSuccess([card3, card4Existing]);

            // Act
            const result = reducer(initialState, createAction);
          
            //Assert
            expect(result).toEqual(expectedResult);
          });

    });


});
