import { createSelector, createFeatureSelector } from '@ngrx/store';
import * as fromCards from './cards';
import * as fromRoot from '../../reducers';
import { Modal } from '../models/modal';
import { Card } from '../models/card';

export interface CardsState {
  cards: fromCards.State;
}

export interface State extends fromRoot.State {
  'cards': CardsState;
}

export const reducers = {
  cards: fromCards.reducer,
};

/**
 * A selector function is a map function factory. We pass it parameters and it
 * returns a function that maps from the larger state tree into a smaller
 * piece of state. This selector simply selects the `books` state.
 *
 * Selectors are used with the `select` operator.
 *
 * ```ts
 * class MyComponent {
 * 	constructor(state$: Observable<State>) {
 * 	  this.booksState$ = state$.select(getBooksState);
 * 	}
 * }
 * ```
 */

/**
 * The createFeatureSelector function selects a piece of state from the root of the state object.
 * This is used for selecting feature states that are loaded eagerly or lazily.
*/
export const getCardsState = createFeatureSelector<CardsState>('cards');

/**
 * Every reducer module exports selector functions, however child reducers
 * have no knowledge of the overall state tree. To make them useable, we
 * need to make new selectors that wrap them.
 *
 * The createSelector function creates very efficient selectors that are memoized and
 * only recompute when arguments change. The created selectors can also be composed
 * together to select different pieces of state.
 */
export const getCardsCollectionState = createSelector(
    getCardsState,
  state => state.cards
);

export const getCardsLoaded = createSelector(
    getCardsCollectionState,
    fromCards.getLoaded
);
export const getCardsLoading = createSelector(
    getCardsCollectionState,
    fromCards.getLoading
);
export const getCardsIds = createSelector(
    getCardsCollectionState,
    fromCards.getIds
);
export const getCardsEntities = createSelector(
    getCardsCollectionState,
    fromCards.getEntities
);
export const getCardsCollection = createSelector(
  getCardsEntities,
  getCardsIds,
  (entities, ids) => {
    return ids.map((id) => {
        let cardEntity = entities[id];
        let modal = new Modal(
            cardEntity.modal.id,
            cardEntity.modal.label,
            cardEntity.modal.title,
            cardEntity.modal.text)
        return  new Card(
            cardEntity.id,
            cardEntity.imgLink,
            cardEntity.imgName,
            cardEntity.title,
            cardEntity.summary,
            modal
        );
    });
  }
);

