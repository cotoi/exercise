import { Component, OnInit, ChangeDetectionStrategy, HostBinding } from '@angular/core';
import { Modal } from '../../../cards-feature/models/modal';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as modalAction from '../../actions/modal';
import * as fromModal from '../../reducers';


@Component({
  selector: 'npw-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModalComponent implements OnInit {
  modalData$: Observable<Modal>;
  isVisible$: Observable<boolean>;

  constructor(private store: Store<fromModal.ModalState>) {

    this.modalData$ = this.store.select(fromModal.getModal);
    this.isVisible$ = this.store.select(fromModal.getModalVisible);

  }

  ngOnInit() {
    // we don;t need to unsubscribe as this will live until the app dies
    this.isVisible$.subscribe(this.toggleBodyClass);
  }

  /**
   * Dispatch action to close the modal
   */
  hideModal() {
    this.store.dispatch(new modalAction.Hide());
  }

  /**
   * Toggle modal--open class on body
   * Need to use platform check for AOT
   * @param show 
   */
  toggleBodyClass(show) {
    if (show) {
      document.body.classList.add('modal--open');
    } else {
      document.body.classList.remove('modal--open');
    }
  }

}
