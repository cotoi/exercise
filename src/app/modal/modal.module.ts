import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { reducers } from './reducers';
import { ModalComponent } from './containers/modal/modal.component';
import { ModalContentComponent } from './components/modal-content/modal-content.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    /**
     * StoreModule.forFeature is used for composing state
     * from feature modules. These modules can be loaded
     * eagerly or lazily and will be dynamically added to
     * the existing state.
     */
    StoreModule.forFeature('modal', reducers),

    /**
     * Effects.forFeature is used to register effects
     * from feature modules. Effects can be loaded
     * eagerly or lazily and will be started immediately.
     *
     * All Effects will only be instantiated once regardless of
     * whether they are registered once or multiple times.
     */
    //EffectsModule.forFeature([]),
  ],
  declarations: [ModalComponent, ModalContentComponent],
  exports: [ModalComponent]
})
export class ModalModule { }
