import { Action } from '@ngrx/store';
import { Modal } from '../../cards-feature/models/modal';



export const SHOW_MODAL = '[Modal] Show';
export const HIDE_MODAL = '[Modal] Hide';




/**
 * Load Actions
 */
export class Hide implements Action {
  readonly type = HIDE_MODAL;
}

export class Show implements Action {
  readonly type = SHOW_MODAL;

  constructor(public payload: Modal) {}
}


export type Actions =
  | Show
  | Hide
  ;
