import * as modal from '../actions/modal';
import { Modal } from '../../cards-feature/models/modal';


export interface State {
  visible: boolean;
  modal : Modal;
}

const initialState: State = {
  visible: false,
  modal: null
};

export function reducer(
  state = initialState,
  action: modal.Actions
): State {
  switch (action.type) {
    case modal.SHOW_MODAL: {
      return {
        modal: Object.assign({},state.modal, action.payload),
        visible: true,
      };
    }

    case modal.HIDE_MODAL: { 
      return initialState;
    }


    default: {
      return state;
    }
  }
}

export const getVisible = (state: State) => state.visible;

export const getModal = (state: State) => state.modal;

