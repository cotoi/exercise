import { Component, OnInit, Input, Output, EventEmitter, OnDestroy, ViewChild, ElementRef, AfterViewInit, ChangeDetectionStrategy } from '@angular/core';
import { Modal } from '../../../cards-feature/models/modal';
import { trigger, state, style, transition, animate, query } from '@angular/animations';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/takeUntil';
import { Subscription } from 'rxjs/Subscription';


const KEY_ESC = 27;

@Component({
  selector: 'npw-modal-content',
  templateUrl: './modal-content.component.html',
  styleUrls: ['./modal-content.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('fadeAnimation', [
      
        transition('* => in', [
         style({ opacity: 0 }),
          animate(300, style({ opacity: 1 }))
        ]),
        transition('* => out', [
          animate(300, style({ opacity: 0 }))
        ])
      

    ])
  ]
})
export class ModalContentComponent implements OnInit, OnDestroy, AfterViewInit {
  lastFocusedElement;
  escPressed$: Observable<any> = Observable.fromEvent(document, 'keyup')
    .filter((pressedKey: KeyboardEvent) => pressedKey.keyCode == KEY_ESC);
  escPressedSubscription: Subscription;

  @Input() visible: boolean = false;
  @Input() content: Modal;
  @Output() close: EventEmitter<any> = new EventEmitter();
  @ViewChild("closeButton") closeBtn: ElementRef;
  @ViewChild("modalWrapper") modalWrapper: ElementRef;

  constructor() { }

  ngOnInit() {
    this.saveLastFocusedElement();
    this.closeOnEscPress()
  }

  ngAfterViewInit() {
    this.setFocusOnTheModal();
  }

  ngOnDestroy() {
    this.resetFocusOnLastFocusedElement();

    // No memory leak, unsubscribe when modal is destroyed 
    this.escPressedSubscription.unsubscribe();
  }

  private closeOnEscPress() {
    this.escPressedSubscription = this.escPressed$.subscribe(() => this.close.emit());
  }

  /**
   * Save the last focused element before the modal opening
   * so we can return to it when the modal
   * is closed
   */
  private saveLastFocusedElement() {
    this.lastFocusedElement = document.activeElement;
  }

  private resetFocusOnLastFocusedElement() {
    // set focus on the saved element
    this.lastFocusedElement.focus();
  }

  private setFocusOnTheModal() {

    this.modalWrapper.nativeElement.setAttribute('tabindex', '0');
    this.modalWrapper.nativeElement.focus();
  }

}
