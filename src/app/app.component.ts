import { Component } from '@angular/core';
import { DataService } from './core/services/data.service';
import { Modal } from './cards-feature/models/modal';
import * as modalAction from './modal/actions/modal'
import { Store } from '@ngrx/store';
import * as fromStore from './reducers';

@Component({
  selector: 'npw-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'npw';

  constructor(private store: Store<fromStore.State>){
    
  }

  showModal( modal:Modal){
    this.store.dispatch(new modalAction.Show(modal));
  }

  closeModal(){
    this.store.dispatch(new modalAction.Hide());
  }
}
